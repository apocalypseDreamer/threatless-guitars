import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import ssrRouter from './ssrRouter';
import apiRouter from './api';
import { mongoConnect } from './mongo/utils';

const PORT = process.env.PORT || 8080;

// Express setup
export const app = express();

// prettier-ignore
app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());

app.use(cors());

app.use(ssrRouter);

app.use('/api', apiRouter);

// Mongo connection
mongoConnect();

app.listen(PORT, () => console.log(`listening on port ${PORT}`));
