import path from 'path';
import express from 'express';
import serverRenderer from './middleware/renderer';

const ssrRouter = express.Router();

ssrRouter.use(serverRenderer);

ssrRouter.use(express.static(path.resolve(__dirname, '..', 'build'), { maxAge: '30d' }));

export default ssrRouter;
