import express from 'express';
const router = express.Router();
import { ScalesModel } from '../mongo/models/Scales';
import { getDocs } from '../mongo/utils';

router.get('/', async (req, res) => {
	try {
		const scales = await getDocs(ScalesModel);
		res.status(200)
			.json(scales)
			.end();
	} catch (e) {
		res.status(500).end();
	}
});

router.post('/', async (req, res) => {
	const { body } = req;

	if (Object.values(body).some(prop => prop.length === 0)) {
		return res
			.status(400)
			.send('Received empty field or fields')
			.end();
	}

	if (body.distances.length !== body.degree.length) {
		return res
			.status(400)
			.send('Distances and degree must be same-length arrays')
			.end();
	}

	const creatingResult = await ScalesModel.create({ ...body });

	res.status(201)
		.send('Created')
		.end();
});

export default router;
