import express from 'express';
const router = express.Router();
import { BassTuningsModel, GuitarTuningsModel } from '../mongo/models/Tunings';
import { getDocs } from '../mongo/utils';

router.get('/', async (req, res) => {
	const tunings = {};

	let shouldGetBassTunings = false;
	let shouldGetGuitarTunings = false;

	switch (req.query.instrument) {
		case 'guitar':
			shouldGetGuitarTunings = true;
			break;

		case 'bass':
			shouldGetBassTunings = true;
			break;

		default:
			shouldGetBassTunings = true;
			shouldGetGuitarTunings = true;
			break;
	}

	if (shouldGetBassTunings) {
		try {
			const bassTunings = await getDocs(BassTuningsModel);
			tunings.bass = bassTunings;
		} catch (e) {
			return res.status(500).end();
		}
	}

	if (shouldGetGuitarTunings) {
		try {
			const guitarTunings = await getDocs(GuitarTuningsModel);
			tunings.guitar = guitarTunings;
		} catch (e) {
			return res.status(500).end();
		}
	}

	res.status(200)
		.json(tunings)
		.end();
});

export default router;
