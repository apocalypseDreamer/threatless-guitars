import express from 'express';
const apiRouter = express.Router();

import scales from './api/scales';
import tunings from './api/tunings';

apiRouter.use('/scales', scales);
apiRouter.use('/tunings', tunings);

export default apiRouter;
