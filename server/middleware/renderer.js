import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { Provider } from 'react-redux';
import { StaticRouter, matchPath } from 'react-router-dom';
import { ServerStyleSheet, StyleSheetManager } from 'styled-components';
import { store } from '../../src/store/config';
import App from '../../src/App.tsx';
import routes from '../../src/routes';

const path = require('path');
const fs = require('fs');

export default (req, res, next) => {
	const matchedUrl = routes.some(route => matchPath(req.url, route));

	if (!matchedUrl) {
		return next();
	}

	console.log(req.url);

	// point to the html file created by CRA's build tool
	const filePath = path.resolve(__dirname, '../../build', 'index.html');

	fs.readFile(filePath, 'utf8', (err, htmlData) => {
		if (err) {
			console.error(err);
			return res.status(500).end();
		}

		const context = {};

		const sheet = new ServerStyleSheet();
		let renderedApp = '';
		let styleTags = '';

		try {
			// render the app as a string
			renderedApp = ReactDOMServer.renderToString(
				<Provider store={store}>
					<StaticRouter location={req.url} context={context}>
						<StyleSheetManager sheet={sheet.instance}>
							<App />
						</StyleSheetManager>
					</StaticRouter>
				</Provider>
			);

			styleTags = sheet.getStyleTags();
		} catch (e) {
			console.log(e);
			res.send(htmlData);
		} finally {
			sheet.seal();
		}

		// inject the rendered app into our html and send it
		return res.send(
			htmlData.replace('<div id="root"></div>', `<div id="root">${renderedApp}</div>`).replace('</head>', `${styleTags}</head>`)
		);
	});
};
