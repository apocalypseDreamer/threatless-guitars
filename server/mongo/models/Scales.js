import { Schema, model } from 'mongoose';

export const Scale = new Schema({
	name: String,
	degree: [String],
	distances: [Number],
});

export const ScalesModel = model('scales', Scale);
