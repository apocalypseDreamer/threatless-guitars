import { Schema, model } from 'mongoose';

export const Tuning = new Schema({
	name: String,
	notes: [String],
});

export const GuitarTuningsModel = model('guitar-tunings', Tuning);
export const BassTuningsModel = model('bass-tunings', Tuning);
