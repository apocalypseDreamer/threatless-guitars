import mongoose from 'mongoose';

const mongoURI = 'mongodb://threatless-admin:Ibanez01mLab@ds331198.mlab.com:31198/threatless-guitars';

// Connection
export const mongoConnect = () => {
	mongoose.connect(mongoURI, { useNewUrlParser: true, useUnifiedTopology: true });
	const db = mongoose.connection;
	db.on('error', console.error.bind(console, 'connection error'));
	db.once('open', () => {
		console.log('Connected to mLab');
	});
};

export const getDocs = (Model, searchParams = {}) =>
	new Promise((resolve, reject) => {
		Model.find(searchParams, (err, docs) => {
			if (err) {
				console.error(err);
				reject(err);
			}

			resolve(docs);
		});
	});
