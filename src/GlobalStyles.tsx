import { createGlobalStyle } from 'styled-components';
import { Colors } from 'design';

const GlobalStyle = createGlobalStyle`
   @import url('https://fonts.googleapis.com/css?family=Neucha');

h1,
h2 {
   margin: 0 auto;
}

h1 {
   font-size: 4em;
}

h2 {
   font-size: 2rem;
}

h3 {
   font-size: 1.8rem;
}

*,
*::before,
*::after {
   box-sizing: border-box;
   margin: 0;
   padding: 0;
}

body {
   /*! body is ancestor of sticky menu which requires its ancestor to be positioned relatively, so it would work all the time  */
   position: relative;
   overflow-x: hidden;
   width: 100%;
   scroll-behavior: smooth;
   font-family: 'Neucha', cursive;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  color: ${Colors.Alto};
}

code {
  font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
    monospace;
}

body > div:not(#root) {
   width: 100%;
   padding: 10px 5px;
   text-align: center;
   background: #ca0505;
   color: #eee;
   font-weight: bold;
   font-size: 4vmin;
   text-shadow: 2px 2px 6px #222;
}
`;

export default GlobalStyle;
