import { createSelector } from 'reselect';
import { Store } from './config';

interface FetchAction<D> {
	type: any;
	data?: D;
	isFetching: boolean;
	hasError: boolean;
}

export const createFetchReducer = <S, D>(initialState: S, actionsEnum: any) => {
	return (state: S = initialState, action: FetchAction<D>): S => {
		switch (action.type) {
			case actionsEnum.RequestData:
				return {
					...state,
					isFetching: true,
					hasError: false,
				};

			case actionsEnum.SetData:
				return {
					...state,
					isFetching: false,
					...action.data,
				};

			case actionsEnum.SetError:
				return {
					...state,
					isFetching: false,
					hasError: true,
				};

			default:
				return state;
		}
	};
};

export const getStateForModule = <S>(state: Store, moduleSelector: (state: Store) => S, fieldName: keyof S) =>
	moduleSelector(state)[fieldName];
