import * as C from './constants';
import { Instrument } from 'api/apiModels';

export const setScale = (scale: C.Scale): C.ScaleDrawerAction => ({
	type: C.ScaleDrawerActionsType.setScale,
	scale,
});

export const setRootNote = (rootNote: C.RootNote): C.ScaleDrawerAction => ({
	type: C.ScaleDrawerActionsType.setRootNote,
	rootNote,
});

export const setInstrument = (instrument: Instrument): C.ScaleDrawerAction => ({
	type: C.ScaleDrawerActionsType.setInstrument,
	instrument,
});

export const setTuning = (tuning: string): C.ScaleDrawerAction => ({
	type: C.ScaleDrawerActionsType.setTuning,
	tuning,
});

export const setIsCapo = (isCapo: boolean): C.ScaleDrawerAction => ({
	type: C.ScaleDrawerActionsType.setIsCapo,
	isCapo,
});

export const setCapoLocation = (capoLocation: number): C.ScaleDrawerAction => ({
	type: C.ScaleDrawerActionsType.setCapoLocation,
	capoLocation,
});

export const setToggleResult = (toggleResult: boolean): C.ScaleDrawerAction => ({
	type: C.ScaleDrawerActionsType.setToggleResult,
	toggleResult,
});

export const setButtonShown = (buttonShown: boolean): C.ScaleDrawerAction => ({
	type: C.ScaleDrawerActionsType.setButtonShown,
	buttonShown,
});
