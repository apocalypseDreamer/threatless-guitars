import { createSelector } from 'reselect';
import { ScaleDrawerState } from './constants';
import { Store } from '../config';
import { getStateForModule } from 'store/utils';

export const scaleDrawerSelector = (state: Store) => state.scaleDrawer;

export const getStateForScaleDrawer = (fieldName: keyof ScaleDrawerState, state: Store) =>
	getStateForModule<ScaleDrawerState>(state, scaleDrawerSelector, fieldName);

export const scaleSelector = createSelector(
	scaleDrawerSelector,
	(scaleDrawerState: ScaleDrawerState) => scaleDrawerState.scale
);

export const rootNoteSelector = createSelector(
	scaleDrawerSelector,
	(scaleDrawerState: ScaleDrawerState) => scaleDrawerState.rootNote
);

export const instrumentSelector = createSelector(
	scaleDrawerSelector,
	(scaleDrawerState: ScaleDrawerState) => scaleDrawerState.instrument
);

export const tuningSelector = createSelector(
	scaleDrawerSelector,
	(scaleDrawerState: ScaleDrawerState) => scaleDrawerState.tuning
);

export const isCapoSelector = createSelector(
	scaleDrawerSelector,
	(scaleDrawerState: ScaleDrawerState) => scaleDrawerState.isCapo
);

export const capoLocationSelector = createSelector(
	scaleDrawerSelector,
	(scaleDrawerState: ScaleDrawerState) => scaleDrawerState.capoLocation
);

export const toggleResultSelector = createSelector(
	scaleDrawerSelector,
	(scaleDrawerState: ScaleDrawerState) => scaleDrawerState.toggleResult
);

export const buttonShownSelector = createSelector(
	scaleDrawerSelector,
	(scaleDrawerState: ScaleDrawerState) => scaleDrawerState.buttonShown
);
