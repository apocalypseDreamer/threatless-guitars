import { scales } from 'scales';
import { Instrument } from 'api/apiModels';

export type RootNote = 'C' | 'C#' | 'D' | 'D#' | 'E' | 'F' | 'F#' | 'G' | 'G#' | 'A' | 'A#' | 'B';

export interface Scale {
	name: string;
	degree: string[];
	distances: number[];
}

export interface ScaleDrawerState {
	scale: Scale;
	rootNote: RootNote;
	instrument: Instrument;
	tuning: string;
	isCapo: boolean;
	capoLocation: number;
	toggleResult: boolean;
	buttonShown: boolean;
}

export const ScaleDrawerInitialState: ScaleDrawerState = {
	scale: scales[0],
	rootNote: 'C',
	instrument: 'guitar',
	tuning: 'E standard',
	isCapo: false,
	capoLocation: 0,
	toggleResult: false,
	buttonShown: false,
};

export enum ScaleDrawerActionsType {
	setScale = 'ScaleDrawer/SET_SCALE',
	setRootNote = 'ScaleDrawer/SET_ROOT_NOTE',
	setInstrument = 'ScaleDrawer/SET_INSTRUMENT',
	setTuning = 'ScaleDrawer/SET_TUNING',
	setIsCapo = 'ScaleDrawer/SET_IS_CAPO',
	setCapoLocation = 'ScaleDrawer/SET_CAPO_LOCATION',
	setToggleResult = 'ScaleDrawer/SET_TOGGLE_RESULT',
	setButtonShown = 'ScaleDrawer/SET_BUTTON_SHOWN',
}

export type ScaleDrawerAction =
	| {
			type: ScaleDrawerActionsType.setScale;
			scale: Scale;
	  }
	| {
			type: ScaleDrawerActionsType.setRootNote;
			rootNote: RootNote;
	  }
	| {
			type: ScaleDrawerActionsType.setInstrument;
			instrument: Instrument;
	  }
	| {
			type: ScaleDrawerActionsType.setTuning;
			tuning: string;
	  }
	| {
			type: ScaleDrawerActionsType.setIsCapo;
			isCapo: boolean;
	  }
	| {
			type: ScaleDrawerActionsType.setCapoLocation;
			capoLocation: number;
	  }
	| {
			type: ScaleDrawerActionsType.setToggleResult;
			toggleResult: boolean;
	  }
	| {
			type: ScaleDrawerActionsType.setButtonShown;
			buttonShown: boolean;
	  };
