import { Reducer } from 'redux';
import * as C from './constants';

export const scaleDrawerReducer: Reducer<C.ScaleDrawerState, C.ScaleDrawerAction> = (
	state: C.ScaleDrawerState = C.ScaleDrawerInitialState,
	action: C.ScaleDrawerAction
): C.ScaleDrawerState => {
	switch (action.type) {
		case C.ScaleDrawerActionsType.setScale:
			return {
				...state,
				scale: action.scale,
			};

		case C.ScaleDrawerActionsType.setRootNote:
			return {
				...state,
				rootNote: action.rootNote,
			};

		case C.ScaleDrawerActionsType.setInstrument:
			return {
				...state,
				instrument: action.instrument,
			};

		case C.ScaleDrawerActionsType.setTuning:
			return {
				...state,
				tuning: action.tuning,
			};

		case C.ScaleDrawerActionsType.setIsCapo:
			return {
				...state,
				isCapo: action.isCapo,
			};

		case C.ScaleDrawerActionsType.setCapoLocation:
			return {
				...state,
				capoLocation: action.capoLocation,
			};

		case C.ScaleDrawerActionsType.setToggleResult:
			return {
				...state,
				toggleResult: action.toggleResult,
			};

		case C.ScaleDrawerActionsType.setButtonShown:
			return {
				...state,
				buttonShown: action.buttonShown,
			};

		default:
			return state;
	}
};
