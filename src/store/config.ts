import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';

// STORE FIELDS INTERFACES
import { GlobalState } from './GlobalData/constants';
import { TabsToNotesState } from './TabsToNotes/constants';

import rootSaga from './rootSaga';
import rootReducer from './rootReducer';
import { ScaleDrawerState } from './ScaleDrawer/constants';

export const sagaMiddleware = createSagaMiddleware();

let composeEnhancers;

try {
	process.env.NODE_ENV === 'development'
		? (composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose)
		: (composeEnhancers = compose);
} catch {
	composeEnhancers = compose;
}

export interface Store {
	globals: GlobalState;
	tabsToNotes: TabsToNotesState;
	scaleDrawer: ScaleDrawerState;
}

export const store = createStore(rootReducer, composeEnhancers(applyMiddleware(sagaMiddleware)));

// store.subscribe(() => console.log(store.getState()));
sagaMiddleware.run(rootSaga);
