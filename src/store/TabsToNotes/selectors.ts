import { createSelector } from 'reselect';
import { TabsToNotesState } from './constants';
import { Store } from '../config';

export const tabsToNotesSelector = (state: Store) => state.tabsToNotes;

export const instrumentSelector = createSelector(
	tabsToNotesSelector,
	(tabsToNotesState: TabsToNotesState) => tabsToNotesState.instrument
);

export const tuningSelector = createSelector(
	tabsToNotesSelector,
	(tabsToNotesState: TabsToNotesState) => tabsToNotesState.tuning
);

export const isCapoSelector = createSelector(
	tabsToNotesSelector,
	(tabsToNotesState: TabsToNotesState) => tabsToNotesState.isCapo
);

export const capoLocationSelector = createSelector(
	tabsToNotesSelector,
	(tabsToNotesState: TabsToNotesState) => tabsToNotesState.capoLocation
);

export const preparedTabSelector = createSelector(
	tabsToNotesSelector,
	(tabsToNotesState: TabsToNotesState) => tabsToNotesState.preparedTab
);

export const toggleResultSelector = createSelector(
	tabsToNotesSelector,
	(tabsToNotesState: TabsToNotesState) => tabsToNotesState.toggleResult
);

export const buttonShownSelector = createSelector(
	tabsToNotesSelector,
	(tabsToNotesState: TabsToNotesState) => tabsToNotesState.buttonShown
);

export const isResultAnimatedSelector = createSelector(
	tabsToNotesSelector,
	(tabsToNotesState: TabsToNotesState) => tabsToNotesState.isResultAnimated
);
