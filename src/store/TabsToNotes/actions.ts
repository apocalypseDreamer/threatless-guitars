import * as C from './constants';
import { Instrument } from 'api/apiModels';

export const setInstrument = (instrument: Instrument): C.TabsToNotesAction => ({
	type: C.TabsToNotesActionsType.setInstrument,
	instrument,
});

export const setTuning = (tuning: string): C.TabsToNotesAction => ({
	type: C.TabsToNotesActionsType.setTuning,
	tuning,
});

export const setIsCapo = (isCapo: boolean): C.TabsToNotesAction => ({
	type: C.TabsToNotesActionsType.setIsCapo,
	isCapo,
});

export const setCapoLocation = (capoLocation: number): C.TabsToNotesAction => ({
	type: C.TabsToNotesActionsType.setCapoLocation,
	capoLocation,
});

export const setPreparedTab = (preparedTab: string[]): C.TabsToNotesAction => ({
	type: C.TabsToNotesActionsType.setPreparedTab,
	preparedTab,
});

export const setToggleResult = (toggleResult: boolean): C.TabsToNotesAction => ({
	type: C.TabsToNotesActionsType.setToggleResult,
	toggleResult,
});

export const setButtonShown = (buttonShown: boolean): C.TabsToNotesAction => ({
	type: C.TabsToNotesActionsType.setButtonShown,
	buttonShown,
});

export const setIsResultAnimated = (isResultAnimated: boolean): C.TabsToNotesAction => ({
	type: C.TabsToNotesActionsType.setIsResultAnimated,
	isResultAnimated,
});
