import { Reducer } from 'redux';
import * as C from './constants';

export const tabsToNotesReducer: Reducer<C.TabsToNotesState, C.TabsToNotesAction> = (
	state: C.TabsToNotesState = C.FormInitialState,
	action: C.TabsToNotesAction
): C.TabsToNotesState => {
	switch (action.type) {
		case C.TabsToNotesActionsType.setInstrument:
			return {
				...state,
				instrument: action.instrument,
			};

		case C.TabsToNotesActionsType.setTuning:
			return {
				...state,
				tuning: action.tuning,
			};

		case C.TabsToNotesActionsType.setIsCapo:
			return {
				...state,
				isCapo: action.isCapo,
			};

		case C.TabsToNotesActionsType.setCapoLocation:
			return {
				...state,
				capoLocation: action.capoLocation,
			};

		case C.TabsToNotesActionsType.setPreparedTab:
			return {
				...state,
				preparedTab: action.preparedTab,
			};

		case C.TabsToNotesActionsType.setToggleResult:
			return {
				...state,
				toggleResult: action.toggleResult,
			};

		case C.TabsToNotesActionsType.setButtonShown:
			return {
				...state,
				buttonShown: action.buttonShown,
			};

		case C.TabsToNotesActionsType.setIsResultAnimated:
			return {
				...state,
				isResultAnimated: action.isResultAnimated,
			};

		default:
			return state;
	}
};
