import { Instrument } from 'api/apiModels';

export interface TabsToNotesState {
	instrument: Instrument;
	tuning: string;
	isCapo: boolean;
	capoLocation: number;
	preparedTab: string[];
	toggleResult: boolean;
	buttonShown: boolean;
	isResultAnimated: boolean;
}

export const FormInitialState: TabsToNotesState = {
	instrument: 'guitar',
	tuning: 'E standard',
	isCapo: false,
	capoLocation: 0,
	preparedTab: [],
	toggleResult: false,
	buttonShown: false,
	isResultAnimated: false,
};

export enum TabsToNotesActionsType {
	setInstrument = 'TabsToNotes/SET_INSTRUMENT',
	setTuning = 'TabsToNotes/SET_TUNING',
	setIsCapo = 'TabsToNotes/SET_IS_CAPO',
	setCapoLocation = 'TabsToNotes/SET_CAPO_LOCATION',
	setPreparedTab = 'TabsToNotes/SET_PREPARED_TAB',
	setToggleResult = 'TabsToNotes/SET_TOGGLE_RESULT',
	setButtonShown = 'TabsToNotes/SET_BUTTON_SHOWN',
	setIsResultAnimated = 'TabsToNotes/SET_IS_RESULT_ANIMATED',
}

export type TabsToNotesAction =
	| {
			type: TabsToNotesActionsType.setInstrument;
			instrument: Instrument;
	  }
	| {
			type: TabsToNotesActionsType.setTuning;
			tuning: string;
	  }
	| {
			type: TabsToNotesActionsType.setIsCapo;
			isCapo: boolean;
	  }
	| {
			type: TabsToNotesActionsType.setCapoLocation;
			capoLocation: number;
	  }
	| {
			type: TabsToNotesActionsType.setPreparedTab;
			preparedTab: string[];
	  }
	| {
			type: TabsToNotesActionsType.setToggleResult;
			toggleResult: boolean;
	  }
	| {
			type: TabsToNotesActionsType.setButtonShown;
			buttonShown: boolean;
	  }
	| {
			type: TabsToNotesActionsType.setIsResultAnimated;
			isResultAnimated: boolean;
	  };
