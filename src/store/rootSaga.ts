import { fork, all, Effect } from 'redux-saga/effects';
import { globalDataRequestWatcher } from './GlobalData/sagas';

export default function* rootSaga() {
	const sagas: Array<() => IterableIterator<Effect>> = [globalDataRequestWatcher];

	yield all(sagas.map(fork));
}
