import { combineReducers } from 'redux';
import { globalReducer } from './GlobalData/reducer';
import { tabsToNotesReducer } from './TabsToNotes/reducer';
import { scaleDrawerReducer } from './ScaleDrawer/reducer';

const rootReducer = combineReducers({ globals: globalReducer, tabsToNotes: tabsToNotesReducer, scaleDrawer: scaleDrawerReducer });

export default rootReducer;
