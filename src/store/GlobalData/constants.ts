import { Tunings } from 'api/apiModels';
import { notes } from 'data';

export interface GlobalState {
	tunings: Tunings;
	notes: string[];
	isFetching: boolean;
	hasError: boolean;
}

export const GlobalInitialState: GlobalState = {
	notes,
	tunings: { bass: [], guitar: [] },
	isFetching: false,
	hasError: false,
};

export type GlobalData = { tunings: Tunings };

export enum GlobalActionsType {
	RequestData = 'Global/REQUEST_DATA',
	SetData = 'Global/SET_DATA',
	SetError = 'Global/SET_ERROR',
}

export type GlobalAction =
	| {
			type: GlobalActionsType.RequestData;
	  }
	| {
			type: GlobalActionsType.SetData;
			data: GlobalData;
	  }
	| {
			type: GlobalActionsType.SetError;
	  };
