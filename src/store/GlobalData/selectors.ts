import { createSelector } from 'reselect';
import { Store } from '../config';
import { GlobalState } from './constants';

export const globalsSelector = (state: Store) => state.globals;

export const notesSelector = createSelector(
	globalsSelector,
	(globals: GlobalState) => globals.notes
);
export const tuningsSelector = createSelector(
	globalsSelector,
	(globals: GlobalState) => globals.tunings
);
export const isFetchingSelector = createSelector(
	globalsSelector,
	(globals: GlobalState) => globals.isFetching
);
