import { takeLatest, put, call } from 'redux-saga/effects';
import req from 'api/apiProxy';
import { Tunings } from 'api/apiModels';
import * as C from './constants';
import * as A from './actions';

const getTunings = () =>
	new Promise<any>(async (resolve, reject) => {
		try {
			const scales = await req.get<Tunings>('/api/tunings');
			console.log(scales.data);
			// setTimeout(() => resolve(globalData.data), 1500);
			resolve(scales.data);
		} catch (e) {
			reject(e);
		}
	});

export function* globalDataRequestWorker() {
	try {
		const tunings: Tunings = yield call(getTunings);
		yield put(A.setGlobalData({ tunings }));
	} catch (e) {
		console.error(e);
		yield put(A.setError());
	}
}

export function* globalDataRequestWatcher() {
	yield takeLatest(C.GlobalActionsType.RequestData, globalDataRequestWorker);
}
