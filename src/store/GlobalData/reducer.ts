import * as C from './constants';
import { createFetchReducer } from 'store/utils';

export const globalReducer = createFetchReducer<C.GlobalState, C.GlobalData>(C.GlobalInitialState, C.GlobalActionsType);

// export const globalReducer: Reducer<C.GlobalState, C.GlobalAction> = (
// 	state: C.GlobalState = C.GlobalInitialState,
// 	action: C.GlobalAction
// ): C.GlobalState => {
// 	switch (action.type) {
// 		case C.GlobalActionsType.RequestData:
// 			return {
// 				...state,
// 				isFetching: true,
// 			};

// 		case C.GlobalActionsType.SetData:
// 			return {
// 				...state,
// 				isFetching: false,
// 				...action.globalData,
// 			};

// 		default:
// 			return state;
// 	}
// };
