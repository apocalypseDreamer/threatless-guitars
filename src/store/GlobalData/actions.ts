import * as C from './constants';

export const requestGlobalData = (): C.GlobalAction => ({
	type: C.GlobalActionsType.RequestData,
});

export const setGlobalData = (data: C.GlobalData): C.GlobalAction => ({
	type: C.GlobalActionsType.SetData,
	data,
});

export const setError = (): C.GlobalAction => ({
	type: C.GlobalActionsType.SetError,
});
