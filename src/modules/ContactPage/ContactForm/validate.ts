export default (values: any) => {
	if (!values) {
		return {};
	}

	const errors: any = {};

	if (!values.name) {
		errors.name = 'Enter name';
	}

	return errors;
};
