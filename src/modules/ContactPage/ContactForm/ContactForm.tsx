import React from 'react';
import { Form, FormRenderProps } from 'react-final-form';
import * as P from '../parts';
import InputFormWrapper from 'components/FinalForm/InputFormWrapper';
import Button from 'components/Button';
import validate from './validate';

const submitting = (values: any) => {
	console.log(values);
};

const ContactForm = () => {
	return (
		<Form onSubmit={submitting} validate={validate}>
			{({ handleSubmit }: FormRenderProps) => (
				<form onSubmit={handleSubmit}>
					<P.CustomField label={'Your name: '} name='name' type='text' component={InputFormWrapper} />
					<Button onClick={() => null}>Send message!</Button>
				</form>
			)}
		</Form>
	);
};

export default ContactForm;
