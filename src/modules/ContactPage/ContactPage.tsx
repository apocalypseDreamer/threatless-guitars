import React from 'react';
import * as P from './parts';
import ContactForm from './ContactForm/ContactForm';

const ContactPage = () => (
	<P.ContactWrapper>
		<h2>Contact me!</h2>
		<ContactForm />
	</P.ContactWrapper>
);

export default ContactPage;
