import styled from 'styled-components';
import { Field } from 'react-final-form';

export const ContactWrapper = styled.section`
	width: 80%;
`;

export const CustomField = styled(Field)`
	width: 100%;
`;
