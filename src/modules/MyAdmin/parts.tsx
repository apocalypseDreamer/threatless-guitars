import styled from 'styled-components';
import Input from 'components/Input';

export const Wrapper = styled.div`
	margin: 30px auto;
	padding: 5px;
`;

export const CustomInput = styled(Input)`
	display: block;
	margin-bottom: 5px;
`;
