import React, { Component } from 'react';
import * as P from './parts';
import localInstance from 'api/apiProxy';
import Button from 'components/Button';
import { Scale } from 'store/ScaleDrawer/constants';

interface MyAdminState {
	scales: Scale[];
}
class MyAdmin extends Component<{}, MyAdminState> {
	nameRef = React.createRef<HTMLInputElement>();
	degreeRef = React.createRef<HTMLInputElement>();
	distancesRef = React.createRef<HTMLInputElement>();

	state = {
		scales: [],
	};

	componentDidMount() {
		this.getScales();
	}

	getScales = () => {
		localInstance
			.get<Scale[]>('/api/scales')
			.then(axiosRes => {
				const { data: scales } = axiosRes;
				this.setState({ scales });
			})
			.catch(err => console.log(err));
	};

	onScaleSave = () => {
		const name = this.nameRef.current && this.nameRef.current.value;
		let degree = this.degreeRef.current && this.degreeRef.current.value;
		let distances = this.distancesRef.current && this.distancesRef.current.value;

		const formattedDegree = degree && degree.split(',').map(deg => deg.trim());
		const formattedDistances = distances && distances.split(',').map(dist => Number(dist.trim()));

		const newScale = {
			name,
			degree: formattedDegree,
			distances: formattedDistances,
		};

		console.log(newScale);

		localInstance.post('/api/scales', newScale).then(_ => this.getScales());
	};

	render() {
		const { scales } = this.state;
		return (
			<P.Wrapper>
				{scales.map((scale: Scale, index) => (
					<div key={index}>
						<span>
							{scale.name} - {JSON.stringify(scale.degree)} - {JSON.stringify(scale.distances)}
						</span>
					</div>
				))}
				<P.CustomInput ref={this.nameRef} placeholder='Scale name...' width={300} />
				<P.CustomInput ref={this.degreeRef} placeholder='Scale degree...' width={300} />
				<P.CustomInput ref={this.distancesRef} placeholder='Scale distances...' width={300} />
				<Button onClick={this.onScaleSave}>Add scale!</Button>
			</P.Wrapper>
		);
	}
}

export default MyAdmin;
