import React, { Component } from 'react';
import styled from 'styled-components';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { Transition } from 'react-transition-group';
import { TransitionStatus } from 'react-transition-group/Transition';
import * as P from './parts';
import * as S from 'store/TabsToNotes/selectors';
import { Store } from 'store/config';
import { setIsResultAnimated } from 'store/TabsToNotes/actions';
import { transformSingleString } from 'utils/transformTab/transformTab';

interface ResultProps {
	toggleResult: boolean;
	preparedTab: string[];
	tuning: string;
	isResultAnimated: boolean;
	setIsResultAnimated: (isResultAnimated: boolean) => void;
}

const TransitionWrapper = styled.div`
	min-width: 100%;
`;

class Result extends Component<ResultProps, {}> {
	private readonly resultDivRef = React.createRef<HTMLDivElement>();
	fullyRendered = () => this.props.toggleResult && (this.props.preparedTab.length === 4 || this.props.preparedTab.length === 6);

	componentDidMount() {
		this.centerResult();
	}

	componentDidUpdate() {
		this.centerResult();
	}

	centerResult = () => {
		if (!this.fullyRendered()) return;
		try {
			// if there is nothing to be scrolled horizontally, everything can be justified to center and we can hide the scrollbar;
			// otherwise we need to set its justification to flex-start to avoid cutting some parts of the result
			const resultDiv = (this.resultDivRef as any).current;
			const scrollLeftMax = resultDiv.scrollLeftMax;
			resultDiv.style.justifyContent = scrollLeftMax === 0 ? 'center' : 'flex-start';
			resultDiv.style.alignItems = scrollLeftMax === 0 ? 'center' : 'flex-start';
		} catch (e) {
			console.log(e);
		}
	};

	render() {
		const { preparedTab, isResultAnimated, setIsResultAnimated, toggleResult } = this.props;

		const renderCondition = preparedTab.length === 4 || preparedTab.length === 6;

		const transformedTab = preparedTab.map((singleString: string, index: number) => transformSingleString(singleString, index));

		return (
			<Transition in={toggleResult} timeout={100} mountOnEnter appear>
				{(state: TransitionStatus) => (
					<P.AnimatedWrapper state={state}>
						<TransitionWrapper>
							<Transition in={renderCondition} timeout={50}>
								{(state: TransitionStatus) => (
									<P.AnimatedResult
										state={state}
										ref={this.resultDivRef}
										className={isResultAnimated ? 'animation-active' : ''}
										onAnimationEnd={() => setIsResultAnimated(false)}
									>
										{transformedTab}
									</P.AnimatedResult>
								)}
							</Transition>
						</TransitionWrapper>

						<TransitionWrapper>
							<Transition in={!renderCondition} timeout={50}>
								{(state: TransitionStatus) => (
									<P.AnimatedErrorMsg state={state}>Your tab must have 4 or 6 lines!</P.AnimatedErrorMsg>
								)}
							</Transition>
						</TransitionWrapper>
					</P.AnimatedWrapper>
				)}
			</Transition>
		);
	}
}

const mapStateToProps = (state: Store) => ({
	toggleResult: S.toggleResultSelector(state),
	preparedTab: S.preparedTabSelector(state),
	isResultAnimated: S.isResultAnimatedSelector(state),

	// this field is unused, but it must be monitored, so when the tuning changes, the whole component will Update
	tuning: S.tuningSelector(state),
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
	setIsResultAnimated: (isResultAnimated: boolean) => dispatch(setIsResultAnimated(isResultAnimated)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Result);
