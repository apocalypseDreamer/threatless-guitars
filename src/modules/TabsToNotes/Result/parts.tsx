import styled, { css, keyframes, StyledComponent } from 'styled-components';
import { TransitionStatus } from 'react-transition-group/Transition';
import { Colors } from 'design';

export const ResultBlinkAnimation = keyframes`
0% {
   transform: scaleY(1);
   opacity: 1;
}

40% {
   opacity: 0;
   transform: scaleY(0);
}

100% {
   transform: scaleY(1);
   opacity: 1;
}
`;

const AnimatedComponent = (component: StyledComponent<'div', any>) => styled(component)<{ state: TransitionStatus }>`
	${({ state }) => {
		switch (state) {
			case 'entering':
				return css`
					transform: scaleY(0);
				`;
			case 'entered':
				return css`
					transform: scaleY(1);
					transition-delay: 200ms;
					transition-timing-function: ease-out;
				`;
			case 'exiting':
				return css`
					transform: scaleY(1);
				`;
			case 'exited':
				return css`
					transform: scaleY(0);
					transition-timing-function: ease-in;
				`;
		}
	}}

	&.animation-active {
		animation: ${ResultBlinkAnimation} 400ms ease-in-out;
	}
`;

export const Result = styled.div`
	width: 95vw;
	margin: 10px auto;
	overflow-x: auto;
	overflow-y: hidden;
	display: flex;
	flex-direction: column;
	box-shadow: 0 0 3vmin ${Colors.Whiskey};

	transition-property: transform;
	transition-duration: 200ms;
`;

export const AnimatedResult = AnimatedComponent(Result);

export const Wrapper = styled.div`
	width: 100%;
	/* so the error message wont be clipped */
	min-height: 180px;
	display: flex;
	flex-flow: row nowrap;
	align-items: center;
	overflow: hidden;
	position: relative;

	transition-property: transform;
	transition-duration: 200ms;
`;

export const AnimatedWrapper = styled(Wrapper)<{ state: TransitionStatus }>`
	${({ state }) => {
		switch (state) {
			case 'entering':
				return css`
					transform: scaleY(0);
				`;
			case 'entered':
				return css`
					transform: scaleY(1);
					transition-timing-function: ease-out;
				`;
			case 'exited':
				return css`
					transform: scaleY(0);
					transition-timing-function: ease-in;
				`;
			default:
				return;
		}
	}}
`;

export const ErrorMsg = styled.div`
	font-size: 2em;
	color: ${Colors.Flamingo};
	min-width: 100%;
	position: absolute;
	left: 0;
	z-index: 5;

	transition-property: opacity, transform;
	transition-duration: 200ms;

	@media (min-width: 768px) {
		font-size: 3em;
	}

	@media (min-width: 1200px) {
		font-size: 5em;
	}
`;

export const AnimatedErrorMsg = AnimatedComponent(ErrorMsg);
