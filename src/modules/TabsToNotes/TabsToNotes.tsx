import React, { Component } from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Form from './Form';
import Result from './Result';
import { Tunings } from 'api/apiModels';
import { Store } from 'store/config';
import * as A from 'store/TabsToNotes/actions';
import { tuningsSelector } from 'store/GlobalData/selectors';
import Heading from 'components/Heading';

const Wrapper = styled.div`
	padding: 5px 0 30px;
`;

interface TabsToNotesComponentProps {
	tunings: Tunings;
	resetResult: () => void;
	resetButtonShown: () => void;
	setDefaultInstrument: () => void;
	setDefaultTuning: (tuning: string) => void;
	resetIsCapo: () => void;
	resetCapoLocation: () => void;
	resetPreparedTab: () => void;
}

class TabsToNotes extends Component<TabsToNotesComponentProps> {
	makeCleanupBeforeUnmount = () => {
		const {
			tunings,
			resetResult,
			resetButtonShown,
			setDefaultInstrument,
			setDefaultTuning,
			resetIsCapo,
			resetCapoLocation,
			resetPreparedTab,
		} = this.props;

		resetResult();
		resetButtonShown();
		setDefaultInstrument();
		setDefaultTuning(tunings['guitar'][0].name);
		resetIsCapo();
		resetCapoLocation();
		resetPreparedTab();
	};

	componentWillUnmount() {
		this.makeCleanupBeforeUnmount();
	}

	render() {
		return (
			<Wrapper>
				<Heading>Tabs To Notes</Heading>
				<Form />
				<Result />
			</Wrapper>
		);
	}
}

const mapStateToProps = (state: Store) => ({
	tunings: tuningsSelector(state),
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
	resetResult: () => dispatch(A.setToggleResult(false)),
	resetButtonShown: () => dispatch(A.setButtonShown(false)),
	setDefaultInstrument: () => dispatch(A.setInstrument('guitar')),
	setDefaultTuning: (tuning: string) => dispatch(A.setTuning(tuning)),
	resetIsCapo: () => dispatch(A.setIsCapo(false)),
	resetCapoLocation: () => dispatch(A.setCapoLocation(0)),
	resetPreparedTab: () => dispatch(A.setPreparedTab([])),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(TabsToNotes);
