import styled, { css } from 'styled-components';
import { Colors } from '../../../design';
import { TransitionStatus } from 'react-transition-group/Transition';

export const FormWrapper = styled.div`
	display: flex;
	flex-flow: column nowrap;
	justify-content: center;
	align-items: center;
	color: ${Colors.BonJour};
`;

export const Title = styled.h2``;

export const AnimatedButtonWrapper = styled.div<{ state: TransitionStatus }>`
	transition-property: transform, opacity;
	transition-duration: 200ms;

	${props => {
		const { state } = props;
		switch (state) {
			case 'entering':
				return css`
					transform: translateY(-20px) scale(0.1);
					opacity: 0.1;
				`;
			case 'entered':
				return css`
					transform: translateY(0) scale(1);
					opacity: 1;
					transition-timing-function: ease-out;
				`;

			case 'exiting':
				return css`
					opacity: 1;
					transform: translateY(0) scale(1);
				`;

			case 'exited':
				return css`
					transform: rotate(180deg) translateY(-50px) scale(0.2);
					opacity: 0.01;
					transition-timing-function: ease-in;
				`;
		}
	}}
`;
