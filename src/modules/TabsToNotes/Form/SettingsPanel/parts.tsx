import styled from 'styled-components';

export const ConfigForm = styled.div`
	margin: 10px auto 20px;
	display: flex;
	flex-flow: column nowrap;
	align-items: center;
	justify-content: center;

	@media (min-width: 768px) {
		flex-flow: row;
		padding: 5px;
	}
`;

export const FormRow = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	align-self: flex-start;
	margin: 5px auto;
	font-size: 25px;
	cursor: default;

	@media (min-width: 768px) {
		margin-right: 25px;
		align-self: center;
		margin: 5px;
	}
`;

export const Wrapper = styled.div`
	width: 100%;
	display: flex;
	flex-flow: row nowrap;
	align-items: center;
	overflow: hidden;
	position: relative;
`;
