import React, { Component } from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import * as P from './parts';
import * as S from 'store/TabsToNotes/selectors';
import { Store } from 'store/config';
import { setIsCapo, setCapoLocation, setTuning } from 'store/TabsToNotes/actions';
import { tuningsSelector } from 'store/GlobalData/selectors';
import { Instrument, Tunings } from 'api/apiModels';
import Checkbox from 'components/Checkbox';
import Input from 'components/Input';
import SelectBase from 'components/Select';

interface CapoSetterProps {
	isCapo: boolean;
	capoLocation: number;
	setIsCapo: (isCapo: boolean) => void;
	setCapoLocation: (capoLocation: number) => void;
}

interface TuningSetterProps {
	instrument: Instrument;
	tuning: string;
	tunings: Tunings;
	setTuning: (tuning: string) => void;
}

type SettingsPanelProps = CapoSetterProps & TuningSetterProps;

class SettingsPanel extends Component<SettingsPanelProps, { isChecked: boolean }> {
	private readonly capoLocationRef = React.createRef<HTMLInputElement>();

	state = {
		isChecked: false,
	};

	handleCapoSetter = () => {
		const { isCapo, setIsCapo, setCapoLocation } = this.props;

		this.setState(prevState => ({ isChecked: !prevState.isChecked }));

		// when it is set we need to disable it, and clear capoLocation value
		if (isCapo) {
			setCapoLocation(0);
			setIsCapo(false);
		} else {
			setIsCapo(true);

			// when we're setting isCapo to true, we also check if there is something already in the location input
			// and if it's a number, we're also updating the state of location
			const capoReference = (this.capoLocationRef as any).current;
			const parsedLocationValue = parseInt(capoReference.value);

			if (!isNaN(parsedLocationValue)) {
				setCapoLocation(parsedLocationValue);
			}
		}
	};

	handleCapoLocation = (e: React.ChangeEvent<HTMLInputElement>) => {
		const { isCapo, setCapoLocation } = this.props;
		const newLocation = e.target.value;
		const parsedNewLocation = parseInt(newLocation);

		if (!isNaN(parsedNewLocation) && isCapo) {
			return setCapoLocation(parsedNewLocation);
		}

		// if input field is empty, we need to set it to 0
		!newLocation.trim() && setCapoLocation(0);
	};

	render() {
		const { tuning, tunings, instrument, setTuning } = this.props;

		return (
			<P.Wrapper>
				<P.ConfigForm>
					<P.FormRow>
						<SelectBase label='Tuning:' value={tuning} onChange={e => setTuning(e.target.value)} options={tunings[instrument]} />
					</P.FormRow>

					<P.FormRow>
						<Checkbox label='Capo?' checked={this.state.isChecked} onClick={this.handleCapoSetter} />
						<Input placeholder='Which fret?' onChange={this.handleCapoLocation} ref={this.capoLocationRef} />
					</P.FormRow>
				</P.ConfigForm>
			</P.Wrapper>
		);
	}
}

const mapStateToProps = (state: Store) => ({
	isCapo: S.isCapoSelector(state),
	capoLocation: S.capoLocationSelector(state),
	instrument: S.instrumentSelector(state),
	tuning: S.tuningSelector(state),
	tunings: tuningsSelector(state),
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
	setIsCapo: (isCapo: boolean) => dispatch(setIsCapo(isCapo)),
	setCapoLocation: (capoLocation: number) => dispatch(setCapoLocation(capoLocation)),
	setTuning: (tuning: string) => dispatch(setTuning(tuning)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SettingsPanel);
