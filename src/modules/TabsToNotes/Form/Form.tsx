import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { Store } from 'store/config';
import * as A from 'store/TabsToNotes/actions';
import * as S from 'store/TabsToNotes/selectors';
import * as P from './parts';
import TabInputField from './TabInputField/TabInputField';
import SettingsPanel from './SettingsPanel/SettingsPanel';
import Button from 'components/Button';

export interface FormProps {
	buttonShown: boolean;
	setToggleResult: (toggleResult: boolean) => void;
	setButtonShown: (buttonShown: boolean) => void;
}

class Form extends Component<FormProps> {
	handleTransformButton = () => {
		const { setToggleResult, setButtonShown } = this.props;

		setToggleResult(true);
		setButtonShown(true);
	};

	render() {
		const { buttonShown } = this.props;
		return (
			<>
				<P.FormWrapper>
					<P.Title>Enter your tab below :</P.Title>
					<TabInputField />
					<SettingsPanel />
				</P.FormWrapper>

				{!buttonShown && <Button onClick={this.handleTransformButton}>Transform!</Button>}
			</>
		);
	}
}

const mapStateToProps = (state: Store) => ({
	buttonShown: S.buttonShownSelector(state),
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
	setToggleResult: (toggleResult: boolean) => dispatch(A.setToggleResult(toggleResult)),
	setButtonShown: (buttonShown: boolean) => dispatch(A.setButtonShown(buttonShown)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Form);
