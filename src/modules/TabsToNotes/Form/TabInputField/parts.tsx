import styled from 'styled-components';
import { Colors } from '../../../../design';

export const TextArea = styled.textarea`
	margin-top: 20px;
	padding-top: 15px;
	font-family: 'Courier New', Courier, monospace;
	white-space: pre;
	overflow: scroll;
	width: 80vw;
	border: none;
	border-radius: 5px;
	padding: 15px 10px;
	background-color: ${Colors.Tuna};
	color: #f1f1f1;
	box-shadow: 0 0 3vmin ${Colors.Sapphire};
	margin-bottom: 5px;
`;

export const SmallHeading = styled.h3`
	font-size: 1.8rem;
	margin: 10px auto 0;
`;

export const EnlargedSpan = styled.span`
	color: ${Colors.MorningGlory};
	cursor: pointer;
	font-size: 110%;

	@media (min-width: 760px) {
		font-size: 120%;
	}
`;
