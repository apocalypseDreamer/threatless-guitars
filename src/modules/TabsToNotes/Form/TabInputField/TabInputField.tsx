import React, { Component } from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import * as P from './parts';
import { randomTabsList } from './constants';
import { Tunings, Tuning, Instrument } from 'api/apiModels';
import { Store } from 'store/config';
import * as A from 'store/TabsToNotes/actions';
import { tuningsSelector } from 'store/GlobalData/selectors';
import * as S from 'store/TabsToNotes/selectors';

interface TabFieldStateProps {
	tunings: Tunings;
	instrument: Instrument;
	tuning: string;
	toggleResult: boolean;
	isResultAnimated: boolean;
	preparedTab: string[];
}

interface TabFieldDispatchProps {
	setInstrument: (instrument: Instrument) => void;
	setTuning: (tuning: Tuning) => void;
	setPreparedTab: (preparedTab: string[]) => void;
	setToggleResult: (toggleResult: boolean) => void;
	setIsResultAnimated: (isResultAnimated: boolean) => void;
}

class TabInputField extends Component<TabFieldDispatchProps & TabFieldStateProps> {
	private readonly textAreaRef = React.createRef<HTMLTextAreaElement>();

	handleTabInput = (input: string) => {
		const { tunings, instrument } = this.props;

		const rawTabArr = input.split(/\n/);
		const strippedTab = rawTabArr.filter((line: string) => line.trim() !== '');
		this.props.setPreparedTab(strippedTab);

		if (strippedTab.length === 4) {
			if (instrument === 'bass') return;

			this.props.setInstrument('bass');
			this.props.setTuning(tunings['bass'][0]);
		} else if (strippedTab.length === 6) {
			if (instrument === 'guitar') return;

			this.props.setInstrument('guitar');
			this.props.setTuning(tunings['guitar'][0]);
		}
	};

	randomizeTab = () => {
		const { toggleResult, preparedTab } = this.props;
		const tab = randomTabsList[Math.floor(Math.random() * randomTabsList.length)];
		(this.textAreaRef as any).current.value = tab;
		this.handleTabInput(tab);

		if (!toggleResult || (preparedTab.length !== 4 && preparedTab.length !== 6)) {
			return;
		}

		this.props.setIsResultAnimated(true);
	};

	render() {
		return (
			<>
				<P.TextArea rows={12} cols={120} ref={this.textAreaRef} onChange={e => this.handleTabInput(e.target.value)} />
				<P.SmallHeading>
					Or try out some <P.EnlargedSpan onClick={this.randomizeTab}>random</P.EnlargedSpan> tab
				</P.SmallHeading>
			</>
		);
	}
}

const mapStateToProps = (state: Store) => ({
	tunings: tuningsSelector(state),
	toggleResult: S.toggleResultSelector(state),
	tuning: S.tuningSelector(state),
	instrument: S.instrumentSelector(state),
	isResultAnimated: S.isResultAnimatedSelector(state),
	preparedTab: S.preparedTabSelector(state),
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
	setInstrument: (instrument: Instrument) => dispatch(A.setInstrument(instrument)),
	setTuning: (tuning: Tuning) => dispatch(A.setTuning(tuning.name)),
	setPreparedTab: (preparedTab: string[]) => dispatch(A.setPreparedTab(preparedTab)),
	setToggleResult: (toggleResult: boolean) => dispatch(A.setToggleResult(toggleResult)),
	setIsResultAnimated: (isResultAnimated: boolean) => dispatch(A.setIsResultAnimated(isResultAnimated)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(TabInputField);
