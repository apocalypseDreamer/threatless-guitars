import React from 'react';
import { IconsLicenceWrapper } from './parts';

const IconsLicence = () => (
	<IconsLicenceWrapper>
		Icons made by{' '}
		<a href='https://www.flaticon.com/authors/nikita-golubev' title='Nikita Golubev' target='_blank' rel='noopener noreferrer'>
			Nikita Golubev
		</a>{' '}
		from{' '}
		<a href='https://www.flaticon.com/' title='Flaticon' target='_blank' rel='noopener noreferrer'>
			www.flaticon.com
		</a>{' '}
		is licensed by{' '}
		<a href='http://creativecommons.org/licenses/by/3.0/' title='Creative Commons BY 3.0' target='_blank' rel='noopener noreferrer'>
			CC 3.0 BY
		</a>
	</IconsLicenceWrapper>
);

export default IconsLicence;
