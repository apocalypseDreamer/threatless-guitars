import styled from 'styled-components';
import { Colors } from 'design';
import { Link } from 'react-router-dom';

export const FooterWrapper = styled.footer`
	display: flex;
	flex-flow: column nowrap;
	background: ${Colors.TundoraLight};
	width: 100%;
	color: ${Colors.Alto};
	min-height: 100px;
`;

export const FooterBlock = styled.div`
	display: flex;
	flex-flow: column;
	width: 80%;
	margin: 5px auto;
	min-height: 100px;
`;

export const StyledLink = styled(Link)`
	color: inherit;
	text-decoration: none;

	&:hover {
		text-decoration: underline;
	}
`;

export const IconsLicenceWrapper = styled.span`
	& a {
		color: ${Colors.Alto};
	}
`;

export const Copyright = styled.span`
	text-align: center;
	font-size: 16px;
`;
