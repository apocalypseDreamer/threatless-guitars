import React from 'react';
import IconsLicence from './IconsLicence';
import * as P from './parts';

const Footer: React.FC = () => (
	<P.FooterWrapper>
		<P.FooterBlock>
			<h2>Modules</h2>
			<P.StyledLink to='/tabs-to-notes'>Tabs To Notes</P.StyledLink>
			<P.StyledLink to='scale-drawer'>Scale Drawer</P.StyledLink>
		</P.FooterBlock>
		<IconsLicence />
		<P.Copyright>&copy; 2019 Daniel Krzyzak. All rights reserved</P.Copyright>
	</P.FooterWrapper>
);

export default Footer;
