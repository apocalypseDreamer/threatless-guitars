import React from 'react';
import Form from './Form';
import Result from './Result';
import styled from 'styled-components';
import Heading from 'components/Heading';

const Wrapper = styled.div`
	padding: 5px 0 30px;
`;

const ScaleDrawer: React.FC = () => (
	<Wrapper>
		<Heading>Scale Drawer</Heading>
		<Form />
		<Result />
	</Wrapper>
);

export default ScaleDrawer;
