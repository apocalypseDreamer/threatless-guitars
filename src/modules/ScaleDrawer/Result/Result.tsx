import React, { Component } from 'react';
import { connect } from 'react-redux';
import domToImage from 'dom-to-image';
import * as P from './parts';
import drawScale from 'utils/drawScale/drawScale';
import { store, Store } from 'store/config';
import { toggleResultSelector, scaleDrawerSelector, getStateForScaleDrawer } from 'store/ScaleDrawer/selectors';
import { Scale, RootNote } from 'store/ScaleDrawer/constants';
import { Instrument, Tuning } from 'api/apiModels';

interface ResultProps {
	scale: Scale;
	rootNote: RootNote;
	instrument: Instrument;
	isCapo: boolean;
	capoLocation: number;
	tuning: string;
	toggleResult: boolean;
}

class Result extends Component<ResultProps> {
	render() {
		const { scale, rootNote, instrument, isCapo, capoLocation, tuning, toggleResult } = this.props;

		return (
			<P.Wrapper id='result-wrapper'>
				{toggleResult && <P.Fretboard>{drawScale(scale, rootNote, instrument, isCapo, capoLocation, tuning)}</P.Fretboard>}
			</P.Wrapper>
		);
	}
}

const mapStateToProps = (state: Store) => ({
	scale: getStateForScaleDrawer('scale', state),
	rootNote: getStateForScaleDrawer('rootNote', state),
	instrument: getStateForScaleDrawer('instrument', state),
	isCapo: getStateForScaleDrawer('isCapo', state),
	capoLocation: getStateForScaleDrawer('capoLocation', state),
	tuning: getStateForScaleDrawer('tuning', state),
	toggleResult: getStateForScaleDrawer('toggleResult', state),
});

export default connect(mapStateToProps)(Result as any);
