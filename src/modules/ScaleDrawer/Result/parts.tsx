import styled from 'styled-components';

export const Wrapper = styled.div`
	width: 90vw;
	margin: auto;
`;

export const Fretboard = styled.div`
	display: flex;
	flex-flow: column nowrap;
	width: 100%;
	overflow-x: auto;
	margin: 0 auto;
	padding-bottom: 4px;
`;
