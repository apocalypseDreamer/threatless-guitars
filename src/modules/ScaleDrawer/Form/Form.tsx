import React, { Component } from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import * as P from './parts';
import ScalePicker from './ScalePicker';
import { Instrument, Tunings } from 'api/apiModels';
import { Store } from 'store/config';
import { tuningsSelector } from 'store/GlobalData/selectors';
import * as S from 'store/ScaleDrawer/selectors';
import * as A from 'store/ScaleDrawer/actions';
import SelectBase from 'components/Select';
import Toggle from 'components/Toggle';
import Checkbox from 'components/Checkbox';
import Input from 'components/Input';
import Button from 'components/Button';

interface FormState {
	guitarToggled: boolean;
	capoChecked: boolean;
}

interface FormProps {
	isCapo: boolean;
	capoLocation: number;
	instrument: Instrument;
	tuning: string;
	tunings: Tunings;
	toggleResult: boolean;
	buttonShown: boolean;
	setIsCapo: (isCapo: boolean) => void;
	setCapoLocation: (capoLocation: number) => void;
	setTuning: (tuning: string) => void;
	setInstrument: (instrument: Instrument) => void;
	setToggleResult: (toggleResult: boolean) => void;
	setButtonShown: (buttonShown: boolean) => void;
}

class Form extends Component<FormProps, FormState> {
	private readonly capoLocationRef = React.createRef<HTMLInputElement>();

	state = {
		guitarToggled: true,
		capoChecked: false,
	};

	onInstrumentToggle = () => {
		this.setState(
			({ guitarToggled: prevIsToggled }) => ({
				guitarToggled: !prevIsToggled,
			}),
			() => {
				const { setInstrument, setTuning, tunings } = this.props;
				const { guitarToggled } = this.state;
				const currentInstrument = guitarToggled ? 'guitar' : 'bass';
				setTuning(tunings[currentInstrument][0].name);
				setInstrument(currentInstrument);
			}
		);
	};

	handleCapoSetter = () => {
		const { isCapo, setIsCapo, setCapoLocation } = this.props;

		this.setState(({ capoChecked: prevCapoChecked }) => ({ capoChecked: !prevCapoChecked }));

		// when it is set we need to disable it, and clear capoLocation value
		if (isCapo) {
			setCapoLocation(0);
			setIsCapo(false);
		} else {
			setIsCapo(true);

			// when we're setting isCapo to true, we also check if there is something already in the location input
			// and if it's a number, we're also updating the state of location
			const capoReference = (this.capoLocationRef as any).current;
			const parsedLocationValue = parseInt(capoReference.value);

			if (!isNaN(parsedLocationValue)) {
				setCapoLocation(parsedLocationValue);
			}
		}
	};

	handleCapoLocation = (e: React.ChangeEvent<HTMLInputElement>) => {
		const { isCapo, setCapoLocation } = this.props;
		const newLocation = e.target.value;
		const parsedNewLocation = parseInt(newLocation);

		if (!isNaN(parsedNewLocation) && isCapo) {
			return setCapoLocation(parsedNewLocation);
		}

		// if input field is empty, we need to set it to 0
		!newLocation.trim() && setCapoLocation(0);
	};

	handleTransformButton = () => {
		const { setToggleResult, setButtonShown } = this.props;

		setToggleResult(true);
		setButtonShown(true);
	};

	render() {
		const { guitarToggled, capoChecked } = this.state;
		const { tuning, tunings, instrument, buttonShown, setTuning } = this.props;

		return (
			<P.FormWrapper>
				<P.FormRow fullWidth>
					<Toggle title='Guitar' secondChoice='Bass' isToggled={guitarToggled} onClick={this.onInstrumentToggle} />
				</P.FormRow>

				<P.FormRow fullWidth>
					<ScalePicker />
				</P.FormRow>

				<P.FormRow fullWidth>
					<SelectBase label='Tuning:' value={tuning} onChange={e => setTuning(e.target.value)} options={tunings[instrument]} />
				</P.FormRow>

				<P.FormRow fullWidth>
					<Checkbox label='Capo?' checked={capoChecked} onClick={this.handleCapoSetter} />
					<Input placeholder='Which fret?' onChange={this.handleCapoLocation} ref={this.capoLocationRef} />
				</P.FormRow>

				{!buttonShown && (
					<P.FormRow fullWidth>
						<Button onClick={this.handleTransformButton}>Draw!</Button>
					</P.FormRow>
				)}
			</P.FormWrapper>
		);
	}
}

const mapStateToProps = (state: Store) => ({
	isCapo: S.isCapoSelector(state),
	capoLocation: S.capoLocationSelector(state),
	instrument: S.instrumentSelector(state),
	tuning: S.tuningSelector(state),
	tunings: tuningsSelector(state),
	toggleResult: S.toggleResultSelector(state),
	buttonShown: S.buttonShownSelector(state),
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
	setIsCapo: (isCapo: boolean) => dispatch(A.setIsCapo(isCapo)),
	setCapoLocation: (capoLocation: number) => dispatch(A.setCapoLocation(capoLocation)),
	setTuning: (tuning: string) => dispatch(A.setTuning(tuning)),
	setInstrument: (instrument: Instrument) => dispatch(A.setInstrument(instrument)),
	setToggleResult: (toggleResult: boolean) => dispatch(A.setToggleResult(toggleResult)),
	setButtonShown: (buttonShown: boolean) => dispatch(A.setButtonShown(buttonShown)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Form);
