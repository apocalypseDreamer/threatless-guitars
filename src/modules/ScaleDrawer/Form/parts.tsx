import styled from 'styled-components';

export const FormWrapper = styled.section`
	width: 100%;
	display: flex;
	flex-flow: column nowrap;
	justify-content: center;
	align-items: center;
	margin-bottom: 36px;

	& > * {
		margin-bottom: 20px;
	}

	@media (min-width: 768px) {
		flex-flow: row wrap;
	}
`;

export const FormRow = styled.div<{ fullWidth?: boolean }>`
	display: flex;
	justify-content: center;
	align-items: center;
	align-self: flex-start;
	margin: 10px auto;
	font-size: 25px;
	cursor: default;

	@media (min-width: 768px) {
		margin-right: 25px;
		align-self: center;
		width: ${({ fullWidth }) => (fullWidth ? '100%' : 'calc(100% / 3)')};
		margin: 5px;
	}
`;
