import React, { Component } from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import * as P from './parts';
import { setRootNote, setScale } from 'store/ScaleDrawer/actions';
import { RootNote, Scale } from 'store/ScaleDrawer/constants';
import SelectBase from 'components/Select';
import { scales, rootNotes } from 'scales';
import { Store } from 'store/config';
import { rootNoteSelector, scaleSelector } from 'store/ScaleDrawer/selectors';

interface ScalePickerProps {
	rootNote: RootNote;
	scale: Scale;
	setRootNote: (rootNote: RootNote) => void;
	setScale: (scale: Scale) => void;
}

class ScalePicker extends Component<ScalePickerProps> {
	onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		const {
			target: { value },
		} = e;
		console.log(value);
	};

	onRootNoteSelectChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
		const { setRootNote } = this.props;
		const {
			target: { value: newRootNote },
		} = e;

		setRootNote(newRootNote as RootNote);
	};

	onScaleSelectChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
		const { setScale } = this.props;
		const {
			target: { value: newScaleName },
		} = e;

		const newScale = scales.find(scale => scale.name === newScaleName);
		newScale && setScale(newScale);
	};

	render() {
		const { rootNote, scale } = this.props;
		return (
			<P.ScalePickerWrapper>
				<SelectBase options={rootNotes} value={rootNote} onChange={this.onRootNoteSelectChange} />
				<SelectBase options={scales} value={scale.name} onChange={this.onScaleSelectChange} />
			</P.ScalePickerWrapper>
		);
	}
}

const mapStateToProps = (state: Store) => ({
	rootNote: rootNoteSelector(state),
	scale: scaleSelector(state),
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
	setRootNote: (rootNote: RootNote) => dispatch(setRootNote(rootNote)),
	setScale: (scale: Scale) => dispatch(setScale(scale)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ScalePicker);
