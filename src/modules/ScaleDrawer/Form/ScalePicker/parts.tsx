import styled from 'styled-components';
import Input from 'components/Input';

export const ScalePickerWrapper = styled.div`
	display: flex;
`;

export const ScalePickerInput = styled(Input)``;
