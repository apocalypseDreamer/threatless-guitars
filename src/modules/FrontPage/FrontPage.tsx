import React from 'react';
import * as P from './parts';
import { cards } from './constants';
import Spinner from 'components/Spinner/Spinner';
import Card from 'components/Card';

interface FrontPageProps {
	isLoading: boolean;
	hasError: boolean;
}

const FrontPage: React.FC<FrontPageProps> = ({ isLoading, hasError }) => {
	let renderContent: React.ReactNode;

	if (!hasError && !isLoading) {
		renderContent = (
			<>
				<P.Heading>Select module</P.Heading>
				<P.CardsContainer>
					{cards.map((cardProps, index) => (
						<Card key={index} {...cardProps} />
					))}
				</P.CardsContainer>
			</>
		);
	}

	if (isLoading) {
		renderContent = <Spinner />;
	}

	if (hasError) {
		renderContent = <h2>error occured</h2>;
	}

	return <P.Wrapper>{renderContent}</P.Wrapper>;
};

export default FrontPage;
