import tabsToNotesImg from 'assets/tabs-to-notes.png';
import drawScaleImg from 'assets/draw-scale.png';

interface Card {
	moduleName: string;
	linkUrl: string;
	imageSrc: string;
	description: string;
}

export const cards: Card[] = [
	{
		moduleName: 'Tabs To Notes',
		linkUrl: '/tabs-to-notes',
		imageSrc: tabsToNotesImg,
		description: "This module takes a guitar tab as an input, and in the output you'll get nicely shown what notes are in this tab.",
	},
	{
		moduleName: 'Scale Drawer',
		linkUrl: '/scale-drawer',
		imageSrc: drawScaleImg,
		description: 'In this module you can select any musical scale, and it will be drawn on the fretboard.',
	},
];
