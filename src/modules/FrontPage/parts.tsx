import styled from 'styled-components';
import { Colors } from 'design';

export const Wrapper = styled.main`
	width: 85%;
	min-height: 50vh;
	display: flex;
	flex-flow: column;
	align-items: center;
	background: ${Colors.BlueBell};
	margin: 10px 20px;
	padding: 20px 0;
`;

export const Heading = styled.h2`
	font-size: 48px;
	font-variant: small-caps;
	margin-bottom: 10px;
	text-shadow: -1px 1px 1px ${Colors.Voodoo};

	@media (min-width: 768px) {
		font-size: 66px;
	}

	@media (min-width: 1024px) {
		font-size: 80px;
	}
`;

export const CardsContainer = styled.div`
	display: flex;
	flex-flow: column nowrap;
	align-items: center;
	justify-content: space-between;

	@media (min-width: 1024px) {
		flex-flow: row wrap;
		justify-content: space-evenly;
	}
`;
