import styled from 'styled-components';
import { NavLink } from 'react-router-dom';
import { Colors } from 'design';

export const HeaderWrapper = styled.header`
	display: flex;
	flex-flow: column nowrap;
	width: 100%;
	color: ${Colors.Alto};
`;

export const Front = styled.div`
	display: flex;
	flex-flow: column nowrap;
	justify-content: center;
	align-items: center;
	width: 100%;
	padding: 20px 5px 0;
	background: linear-gradient(150deg, ${Colors.Mandy}, ${Colors.Mandy} 5%, ${Colors.Rhino} 40% 65%, ${Colors.Mandy});
`;

export const FrontHeading = styled.p`
	display: block;
	width: 100%;
	padding-top: 20px;
	font-size: 40px;
	text-shadow: 2px 2px 0px #111;

	@media (min-width: 768px) {
		font-size: 72px;
	}

	@media (min-width: 1024px) {
		font-size: 92px;
	}
`;

export const FrontLead = styled.p`
	width: 70%;
	font-size: 20px;
	color: ${Colors.Gallery};
	padding: 10px 0;

	@media (min-width: 768px) {
		font-size: 30px;
	}
`;

export const LogoContainer = styled.div`
	display: flex;
	transform: rotate(28deg);
`;

export const MainNav = styled.nav`
	display: flex;
	width: 100vw;
	border-top: 3px solid ${Colors.Gallery};
	padding: 5px;
	justify-content: space-evenly;
`;

export const StyledLink = styled(NavLink)`
	display: flex;
	justify-content: center;
	align-items: center;
	width: calc((100% / 3) - 4vw);
	padding: 10px 16px;
	border: 2px solid transparent;
	border-radius: 15px;
	color: #eee;
	text-decoration: none;
	font-size: 18px;
	cursor: pointer;
	transition: background-color 200ms ease-out;

	&:hover {
		border: 2px solid ${Colors.AltoDarker};
		background-color: ${Colors.WhiteTransparent};
	}

	&.selected {
		background-color: ${Colors.WhiteTransparent};
	}

	@media (min-width: 768px) {
		font-size: 24px;
	}

	@media (min-width: 1024px) {
		font-size: 28px;
	}
`;
