import React from 'react';
import * as P from './parts';
import Logo from 'components/Icons/Logo/Logo';

const Header: React.FC = () => (
	<P.HeaderWrapper>
		<P.Front>
			<P.FrontHeading>Threatless Guitars</P.FrontHeading>
			<Logo width={100} height={100} />
			<P.FrontLead>Make music theory no threat anymore!</P.FrontLead>
			<P.MainNav>
				<P.StyledLink to='/' exact activeClassName='selected'>
					Main Page
				</P.StyledLink>
				<P.StyledLink to='/about' activeClassName='selected'>
					About
				</P.StyledLink>
				<P.StyledLink to='/contact' activeClassName='selected'>
					Contact
				</P.StyledLink>
			</P.MainNav>
		</P.Front>
	</P.HeaderWrapper>
);

export default Header;
