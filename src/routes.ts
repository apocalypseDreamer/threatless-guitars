import FrontPage from './modules/FrontPage/FrontPage';
import React from 'react';
import TabsToNotes from './modules/TabsToNotes/TabsToNotes';
import { RouteProps } from 'react-router';
import { store } from './store/config';
import ScaleDrawer from 'modules/ScaleDrawer/ScaleDrawer';
import ContactPage from 'modules/ContactPage/ContactPage';
import MyAdmin from 'modules/MyAdmin/MyAdmin';

let isFetchingGlobalData = false;
let hasErrorGlobalData = false;

store.subscribe(() => {
	isFetchingGlobalData = store.getState().globals.isFetching;
	hasErrorGlobalData = store.getState().globals.hasError;
});

const routes: RouteProps[] = [
	{
		path: '/',
		exact: true,
		render: () => React.createElement(FrontPage, { isLoading: isFetchingGlobalData, hasError: hasErrorGlobalData }),
	},
	{
		path: '/contact',
		component: ContactPage,
	},
	{
		path: '/tabs-to-notes',
		component: TabsToNotes,
	},
	{
		path: '/scale-drawer',
		component: ScaleDrawer,
	},
	{
		path: '/admin',
		component: MyAdmin,
	},
];

export default routes;
