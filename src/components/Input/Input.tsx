import React from 'react';
import * as P from './parts';

interface InputProps {
	placeholder: string;
	onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
	width?: number;
	[key: string]: any;
}

const Input = React.forwardRef<HTMLInputElement, InputProps>((props, ref) => <P.Input ref={ref} {...props} />);

export default Input;
