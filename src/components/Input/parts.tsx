import styled from 'styled-components';
import { Colors } from 'design';

interface InputProps {
	width?: number;
}

export const Input = styled.input`
	margin-left: 10px;
	width: ${({ width }: InputProps) => `${width || 144}px`};
	font-size: 20px;
	display: inline-block;
	border-radius: 15px;
	outline: none;
	border: 1px solid ${Colors.BlueMarguerite};
	padding: 5px 10px;
`;
