import styled from 'styled-components';
import { Colors } from 'design';

interface SwitchProps {
	isToggled: boolean;
}

export const ToggleWrapper = styled.div`
	width: 200px;
	display: flex;
	align-items: center;
	justify-content: center;

	@media (min-width: 1024px) {
		width: 360px;
	}
`;

export const Title = styled.p`
	font-size: 26px;

	@media (min-width: 1024px) {
		font-size: 36px;
	}
`;

export const Switch = styled.div<SwitchProps>`
	width: 66px;
	height: 36px;
	margin: 0 12px;
	border-radius: 36px;
	display: flex;
	align-items: center;
	transition: background-color 200ms ease-in-out;
	background: ${({ isToggled }) => (isToggled ? Colors.BlueMarguerite : Colors.CapePalliser)};
`;

export const Slider = styled.div<SwitchProps>`
	border-radius: 50%;
	width: 30px;
	height: 30px;
	background-color: ${Colors.AthensGray};
	transform: translateX(${({ isToggled }) => (isToggled ? '6px' : '30px')});
	transition: transform 200ms ease-in-out;
`;
