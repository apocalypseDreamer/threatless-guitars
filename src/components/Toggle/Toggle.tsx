import React from 'react';
import * as P from './parts';

interface ToggleProps {
	isToggled: boolean;
	onClick: () => void;
	title: string;
	secondChoice?: string;
}

const Toggle: React.FC<ToggleProps> = ({ isToggled, onClick, title, secondChoice }) => {
	return (
		<P.ToggleWrapper>
			<P.Title>{title}</P.Title>
			<P.Switch isToggled={isToggled} onClick={onClick}>
				<P.Slider isToggled={isToggled} />
			</P.Switch>
			{secondChoice && <P.Title>{secondChoice}</P.Title>}
		</P.ToggleWrapper>
	);
};

export default Toggle;
