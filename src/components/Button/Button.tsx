import React from 'react';
import * as P from './parts';

interface ButtonProps {
	onClick: () => void;
	type?: 'submit' | 'reset' | 'button';
}

const Button: React.FC<ButtonProps> = ({ children, onClick, type = 'submit', ...restProps }) => {
	return (
		<P.Button type={type} onClick={onClick} {...restProps}>
			{children}
		</P.Button>
	);
};

export default Button;
