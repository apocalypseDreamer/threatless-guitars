import styled from 'styled-components';
import { Colors } from 'design';

export const Button = styled.button`
	width: 200px;
	height: 60px;
	margin: 0 auto 10px;
	border: 2px solid ${Colors.HummingBird};
	border-radius: 15px;
	font-family: 'Neucha', cursive;
	text-align: center;
	text-decoration: none;
	font-size: 30px;
	line-height: 60px;
	background: ${Colors.BlueMarguerite};
	color: ${Colors.HummingBird};

	transition: color 300ms ease-in, background 250ms ease-in-out, border-color 250ms ease-out;
	cursor: pointer;

	&:hover {
		color: ${Colors.BlueMarguerite};
		background: ${Colors.HummingBird};
		border-color: ${Colors.BlueMarguerite};
	}
`;
