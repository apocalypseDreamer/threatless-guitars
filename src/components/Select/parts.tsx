import styled from 'styled-components';
import { Colors } from 'design';

export const SelectWrapper = styled.div`
	display: flex;
	align-items: center;
`;

export const Label = styled.label`
	font-size: 25px;
`;

export const Select = styled.select`
	font-size: 20px;
	display: inline-block;
	margin-left: 10px;
	border-radius: 15px;
	border: 1px solid ${Colors.BlueMarguerite};
	padding: 5px 10px;
`;
