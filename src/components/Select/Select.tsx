import React from 'react';
import * as P from './parts';
import { SelectProps } from './constants';

const SelectBase: React.FC<SelectProps> = ({ label = '', value, onChange, options }) => (
	<P.SelectWrapper>
		<P.Label>{label} </P.Label>
		<P.Select value={value} onChange={onChange}>
			{options.map((selectItem, index) => {
				if (typeof selectItem === 'number' || typeof selectItem === 'string') {
					return (
						<option key={index} value={selectItem}>
							{selectItem}
						</option>
					);
				}

				const { id, name, value } = selectItem;
				return (
					<option key={id || index} value={value || name}>
						{value || name}
					</option>
				);
			})}
		</P.Select>
	</P.SelectWrapper>
);

export default SelectBase;
