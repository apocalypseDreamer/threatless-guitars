export type Value = { value: string | number };
export type Name = { name: string | number };
export type ValueToMap = Value | Name;

export interface SelectProps<V = string> {
	onChange: (e: any) => void;
	value: V;
	label?: string;
	options: Array<
		| string
		| number
		| ValueToMap & {
				id?: string | number;
				[key: string]: any;
		  }
	>;
}
