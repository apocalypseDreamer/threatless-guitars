import styled, { css } from 'styled-components';
import { Colors } from 'design';

interface CheckboxProps {
	checked: boolean;
}

export const CustomCheckbox = styled.div`
	margin-left: 10px;
	height: 28px;
	width: 28px;
	display: inline-block;
	position: relative;
	border-radius: 4px;

	&::after {
		box-sizing: content-box;
		content: '';
		position: absolute;
		left: 9px;
		top: 5px;
		width: 5px;
		height: 10px;
		border: solid white;
		border-width: 0 3px 3px 0;
		transform: rotate(45deg);
	}
`;

export const CheckboxLabel = styled.label`
	display: inline-block;
	margin-right: 10px;
	font-size: 25px;
	position: relative;
	display: flex;
	justify-content: center;
	cursor: pointer;

	${(props: CheckboxProps) =>
		props.checked
			? css`
					${CustomCheckbox} {
						background-color: ${Colors.HavelockBlue};
					}
			  `
			: css`
					${CustomCheckbox} {
						background-color: ${Colors.Silver};

						&::after {
							display: none;
						}
					}

					&:hover ${CustomCheckbox} {
						background-color: ${Colors.SilverChalice};
					}
			  `}
`;
