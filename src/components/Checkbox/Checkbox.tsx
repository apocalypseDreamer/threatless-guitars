import React from 'react';
import * as P from './parts';

interface CheckboxProps {
	label: string;
	checked: boolean;
	onClick: () => void;
}

const Checkbox: React.FC<CheckboxProps> = ({ label, checked, onClick }) => (
	<P.CheckboxLabel onClick={onClick} checked={checked}>
		{label}
		<P.CustomCheckbox />
	</P.CheckboxLabel>
);

export default Checkbox;
