import React from 'react';
import * as P from './parts';
import { Colors } from 'design';

const ColorsPalette: React.FC = () => (
	<P.PaletteWrapper>
		<P.Heading>Colors Palette</P.Heading>
		<P.ColorsWrapper>
			{Object.keys(Colors).map((colorName, idx) => (
				<P.SingleColorWrapper key={idx}>
					<P.Text>{colorName}</P.Text>
					<P.ColorBox colorValue={Colors[colorName as any]} />
				</P.SingleColorWrapper>
			))}
		</P.ColorsWrapper>
	</P.PaletteWrapper>
);

export default ColorsPalette;
