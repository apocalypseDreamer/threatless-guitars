import styled from 'styled-components';
import { Colors } from 'design';

export const PaletteWrapper = styled.div`
	width: 100%;
	padding: 10px 30px;
	background: white;
`;

export const Heading = styled.h2`
	font-size: 30px;
	font-variant: small-caps;
	color: ${Colors.Tundora};
	display: block;
	width: 100%;

	@media (min-width: 1024px) {
		font-size: 50px;
	}
`;

export const ColorsWrapper = styled.div`
	width: 100%;
	display: flex;
	flex-flow: row wrap;
`;

export const SingleColorWrapper = styled.div`
	width: 100%;
	height: 48px;
	display: flex;
	align-items: center;
	justify-content: space-between;
	border-bottom: 2px solid ${Colors.MineShaft};

	&:first-child {
		border-top: 2px solid ${Colors.MineShaft};
	}

	@media (min-width: 1024px) {
		font-size: 48px;
		width: calc(100% / 2 - 60px);
	}
`;

export const Text = styled.div`
	width: 50%;
	height: 100%;
	font-size: 36px;
	color: ${Colors.MineShaft};
	text-shadow: 1px 1px 1px ${Colors.BlueBell};
	font-weight: bold;
	font-variant: small-caps;
	overflow: hidden;
`;

interface ColorBoxProps {
	colorValue: string;
}

export const ColorBox = styled.div<ColorBoxProps>`
	width: 65%;
	height: 80%;
	background: ${({ colorValue }) => colorValue};

	@media (min-width: 1024px) {
		width: 45%;
	}
`;
