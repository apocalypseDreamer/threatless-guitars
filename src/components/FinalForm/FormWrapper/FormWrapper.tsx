import React from 'react';
import { Form, FormRenderProps, AnyObject } from 'react-final-form';

interface FormWrapperProps {
	validate: (values: AnyObject) => any;
	onSubmit: (values: AnyObject) => void;
}

const FormWrapper: React.FC<FormWrapperProps> = ({ children, onSubmit, validate }) => {
	return <div></div>;
	//   <Form onSubmit={onSubmit} validate={validate}>
	//      {({ handleSubmit }: FormRenderProps) => <form onSubmit={handleSubmit}>{children}</form>}
	//   </Form>
};

export default FormWrapper;
