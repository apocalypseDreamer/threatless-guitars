import styled from 'styled-components';
import { Colors } from 'design';

export const Wrapper = styled.div``;

export const Label = styled.label`
	display: flex;
	justify-content: center;
	align-items: center;
`;

export const LabelText = styled.span`
	width: 100px;
	font-size: 20px;
	margin-right: 10px;
`;

export const Error = styled.span`
	width: 100%;
	color: ${Colors.Flamingo};
`;
