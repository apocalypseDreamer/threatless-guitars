import React from 'react';
import { FieldRenderProps } from 'react-final-form';
import * as P from './parts';
import Input from 'components/Input';

type InputFormWrapperProps = FieldRenderProps<any, HTMLInputElement> & {
	label: string;
};

const InputFormWrapper: React.FC<InputFormWrapperProps> = ({ input, meta, label, ...restProps }) => (
	<P.Wrapper>
		<P.Label>
			<P.LabelText>{label}</P.LabelText>
			<Input {...input} {...restProps} />
			{meta.touched && <P.Error>{meta.error}</P.Error>}
		</P.Label>
	</P.Wrapper>
);

export default InputFormWrapper;
