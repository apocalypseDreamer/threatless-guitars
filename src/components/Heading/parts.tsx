import styled from 'styled-components';
import { Colors } from 'design';

export const Heading = styled.h2`
	font-size: 56px;
	padding: 10px 0;
	text-shadow: 1px 1px 1px ${Colors.Tuna};
	color: ${Colors.BonJour};
	font-variant: small-caps;

	@media (min-width: 1024px) {
		font-size: 84px;
	}
`;
