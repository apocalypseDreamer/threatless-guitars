import React from 'react';
import * as P from './parts';

const Heading: React.FC = ({ children }) => <P.Heading>{children}</P.Heading>;

export default Heading;
