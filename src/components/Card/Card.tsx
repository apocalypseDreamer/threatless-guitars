import React from 'react';
import * as P from './parts';

interface CardProps {
	moduleName: string;
	linkUrl: string;
	imageSrc: string;
	description: string;
}

const Card: React.FC<CardProps> = ({ moduleName, linkUrl, imageSrc, description }) => {
	return (
		<P.Wrapper to={linkUrl}>
			<P.CardName>{moduleName}</P.CardName>
			<P.ImageWrapper>
				<P.Image src={imageSrc} alt='' />
			</P.ImageWrapper>

			<P.Description>{description}</P.Description>
		</P.Wrapper>
	);
};

export default Card;
