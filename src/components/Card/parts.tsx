import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Colors } from 'design';

export const Wrapper = styled(Link)`
	width: 80%;
	display: flex;
	flex-flow: column;
	align-items: center;
	text-decoration: none;
	color: ${Colors.Gondola};
	border: 3px solid ${Colors.TrendyPink};
	border-radius: 15px;
	padding: 10px 5px;
	margin-bottom: 30px;

	@media (min-width: 1024px) {
		width: calc((100% - 100px) / 2);
	}
`;

export const CardName = styled.h3`
	font-size: 36px;

	@media (min-width: 768px) {
		font-size: 44px;
	}

	@media (min-width: 1200px) {
		font-size: 52px;
	}
`;

export const ImageWrapper = styled.div`
	padding: 5px;
`;

export const Description = styled.p`
	font-size: 26px;
	text-align: justify;
	width: 90%;
	text-shadow: 0 0 1px ${Colors.MorningGlory};

	@media (min-width: 768px) {
		font-size: 40px;
	}
`;

export const Image = styled.img`
	width: 90%;
	height: auto;
`;
