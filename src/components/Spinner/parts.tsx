import styled, { css, keyframes } from 'styled-components';

interface RectProps {
	delay?: number;
}

const LoaderAnimation = keyframes`
0%,
      40%,
      100% {
         transform: scaleY(0.4);
      }
      20% {
         transform: scaleY(1);
      }
`;

export const Rect = styled.div`
	background-color: #333;
	height: 60px;
	width: 10px;
	margin-left: 5px;

	animation: ${LoaderAnimation} 1.2s infinite ease-in-out;
	${({ delay }: RectProps) =>
		delay
			? css`
					animation-delay: ${delay}s;
			  `
			: null}
`;

export const Spinner = styled.div`
	width: 50%;
	height: 50%;
	display: flex;
	justify-content: center;
	align-items: center;
	text-align: center;
	font-size: 10px;
`;
