import React from 'react';
import * as P from './parts';

const Spinner = () => {
	return (
		<P.Spinner>
			<P.Rect />
			<P.Rect delay={-1.1} />
			<P.Rect delay={-1.0} />
			<P.Rect delay={-0.9} />
			<P.Rect delay={-0.8} />
		</P.Spinner>
	);
};

export default Spinner;
