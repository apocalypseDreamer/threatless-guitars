export enum Colors {
	// DARK:
	Black = '#000',
	Gondola = '#251514',
	MineShaft = '#333',
	Tuna = '#323234',
	Tundora = '#444',
	TundoraLight = '#4a4a4a',
	Emperor = '#555',

	// GREY
	Comet = '#555C73',
	Raven = '#7C848D',
	Manatee = '#8D90AC',

	// LIGHT:
	SilverChalice = '#AAA',
	Silver = '#CCC',
	BonJour = '#D9D5D7',
	Alto = '#DDD',
	AltoDarker = '#EDEDED',
	Gallery = '#EEE',
	AthensGray = '#F1F3F7',

	// TRANSPARENT:
	WhiteTransparent = 'rgba(240, 240, 240, 0.3)',

	// BLUE:
	HummingBird = '#DFF9FB',
	MorningGlory = '#8BC1DB',
	ShipCove = '#6B87B4',
	Sapphire = '#305398',
	KashmirBlue = '#505893',
	Logan = '#B0B0D3',
	BlueBell = '#8F90CC',
	HavelockBlue = '#686DE0',
	BlueMarguerite = '#6063C5',
	BayOfMany = '#30338F',
	Rhino = '#30336B',
	CloudBurst = '#202757',

	// PURPLE:
	Heath = '#4C131D',
	Voodoo = '#623C69',
	TrendyPink = '#7B588B',
	CannonPink = '#854268',
	Cadillac = '#A84967',

	// RED:
	ChestnutRose = '#CB4F66',
	Mandy = '#ED5565',
	Flamingo = '#F04B4B',

	// YELLOW:
	Akaroa = '#D7CCB2',
	Thatch = '#C3A6A1',
	Whiskey = '#CFA468',
	CapePalliser = '#A67D3F',
}
