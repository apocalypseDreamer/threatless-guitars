import React, { Component } from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import styled from 'styled-components';
import { Store } from 'store/config';
import { requestGlobalData } from 'store/GlobalData/actions';
import { isFetchingSelector } from 'store/GlobalData/selectors';
import Header from 'modules/Header';
import Footer from 'modules/Footer';
import GlobalStyle from 'GlobalStyles';
import routes from 'routes';
import { Colors } from 'design';
import ColorsPalette from 'components/ColorsPalette';

interface AppProps {
	isFetchingGlobalData: boolean;
	requestGlobalData: () => void;
}

export const AppWrapper = styled.div`
	display: flex;
	flex-flow: column nowrap;
	justify-content: center;
	align-items: center;
	text-align: center;
	min-width: 320px;
	background-color: ${Colors.CloudBurst};
`;

class App extends Component<AppProps> {
	// history: History = createBrowserHistory();

	componentDidMount() {
		this.props.requestGlobalData();
	}

	render() {
		return (
			<>
				<GlobalStyle />
				<AppWrapper>
					<Header />
					<Switch>
						{routes.map((routeParams, index) => (
							<Route {...routeParams} key={index} />
						))}
						<Route path='/about' />
					</Switch>

					<ColorsPalette />
					<Footer />
				</AppWrapper>
			</>
		);
	}
}

const mapStateToProps = (state: Store) => ({
	isFetchingGlobalData: isFetchingSelector(state),
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
	requestGlobalData: () => dispatch(requestGlobalData()),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(App);
