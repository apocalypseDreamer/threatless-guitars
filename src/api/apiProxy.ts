import axios from 'axios';

const buildBaseUrl = (method: string, host: string, port: string | number) => {
	return `${method}://${host}:${port}`;
};

const localInstance = axios.create({
	baseURL: buildBaseUrl('http', '192.168.0.42', 8080),
});

export default localInstance;
