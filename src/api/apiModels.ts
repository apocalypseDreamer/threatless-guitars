export type Instrument = 'bass' | 'guitar';

export interface Tuning {
	name: string;
	notes: string[];
	_id: string;
	__v: number;
	[key: string]: any;
}
export interface Tunings {
	bass: Tuning[];
	guitar: Tuning[];
}
