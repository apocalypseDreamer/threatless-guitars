import styled, { css } from 'styled-components';
import { Colors } from 'design';

export const Wrapper = styled.div``;

export const Fret = styled.div`
	display: flex;
	flex-flow: row nowrap;
	justify-content: flex-start;
	align-items: center;
	height: 50px;
`;

export const Note = styled.div<{ index: number }>`
	background: ${Colors.BlueMarguerite};
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
	flex: 1 0 ${({ index }) => 150 - 1.2 * (index || 0)}px;
	position: relative;
	font-size: 20px;

	&::before {
		content: '';
		position: absolute;
		top: 50%;
		transform: translateY(-50%);
		left: 0;
		width: 100%;
		height: 6px;
		background: ${Colors.Heath};
		z-index: 5;
	}

	&::after {
		content: '';
		position: absolute;
		top: 0;
		right: 0;
		width: 8px;
		height: 100%;
		background: ${Colors.Whiskey};
		z-index: 2;
	}
`;

export const NoteName = styled.div<{ isRoot: boolean }>`
	width: 36px;
	height: 36px;
	line-height: 36px;
	border-radius: 50%;
	background-color: ${Colors.CloudBurst};
	color: ${({ isRoot }) => (isRoot ? Colors.CapePalliser : Colors.Gallery)};
	font-size: 20px;
	position: relative;
	z-index: 10;

	${({ isRoot }) =>
		isRoot &&
		css`
			font-weight: bold;
		`}
`;

export const FretCounter = styled(Fret)``;

export const FretIndex = styled(Note)`
	background: ${Colors.HummingBird};
	color: ${Colors.CloudBurst};
	justify-self: center;
	align-self: center;

	&::before {
		content: none;
	}

	&::after {
		content: none;
	}
`;
