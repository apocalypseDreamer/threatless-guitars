import React from 'react';
import { scales, rootNotes } from 'scales';
import * as P from './parts';
import { getTuningNotes, findNoteNameFromFretboardLocation } from 'utils/helpers';
import { Scale, RootNote } from 'store/ScaleDrawer/constants';
import { Instrument } from 'api/apiModels';

const drawScale = (scale: Scale, rootNote: RootNote, instrument: Instrument, isCapo: boolean, capoLocation: number, tuning: string) => {
	const tuningNotes = getTuningNotes(instrument, tuning);

	const scaleObj = scales.find(({ name }) => name === scale.name);
	const scaleDistances = (scaleObj && scaleObj.distances) || [];

	const rootNoteIndex = rootNotes.findIndex(note => note === rootNote);
	const notesStartingFromRoot = rootNotes.slice(rootNoteIndex).concat(rootNotes.slice(0, rootNoteIndex));
	const notesInScale = notesStartingFromRoot.filter((_, idx) => scaleDistances.includes(idx));

	const startingFret = (isCapo && capoLocation) || 0;
	const fretboardLength = 25 - startingFret;
	return (
		<>
			<P.FretCounter>
				{new Array(fretboardLength).fill(null).map((_, idx) => (
					<P.FretIndex key={idx} index={idx + startingFret}>
						{idx + startingFret}
					</P.FretIndex>
				))}
			</P.FretCounter>

			{tuningNotes.map((noteOnOpenString, idx) => (
				<P.Fret key={idx}>
					{new Array(fretboardLength).fill(null).map((_, fretIndex) => {
						const note = findNoteNameFromFretboardLocation(noteOnOpenString, fretIndex + startingFret, true);

						return (
							<P.Note key={fretIndex} index={fretIndex + startingFret}>
								{notesInScale.includes(note) && <P.NoteName isRoot={note === rootNote}>{note}</P.NoteName>}
							</P.Note>
						);
					})}
				</P.Fret>
			))}
		</>
	);
};

export default drawScale;
