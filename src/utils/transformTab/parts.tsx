import styled, { css } from 'styled-components';
import { Colors } from 'design';

export const Block = styled.div`
	min-width: 40px;
	height: 40px;
	border-left: 2px solid black;
	border-top: 2px solid black;
	text-align: center;
	line-height: 40px;
	color: ${Colors.Gondola};
	font-weight: bold;

	&.empty {
		background: #6063c5;
	}

	&.note {
		background: #7ed6df;
		${(props: { isDouble?: boolean }) =>
			props.isDouble
				? css`
						min-width: 80px;
				  `
				: null}
	}

	&.limiter {
		background: #30336b;
	}

	&.other {
		background: #dff9fb;
	}
`;

export const String = styled.div`
	display: flex;
	flex-flow: row nowrap;

	&::nth-last-child(1) ${Block} {
		border-bottom: 2px solid black;
	}
`;
