import React from 'react';
import * as P from './parts';
import { store } from 'store/config';
import { getTuningNotes, findNoteNameFromFretboardLocation } from 'utils/helpers';

export const transformSingleString = (singleStringNotation: string, index: number) => {
	const {
		tabsToNotes: { tuning, instrument, capoLocation },
	} = store.getState();

	const tuningNotes = getTuningNotes(instrument, tuning);

	// the note while playing open string
	const defaultNote = tuningNotes[index];

	// every char from the string notation
	const chars = singleStringNotation.split('');

	// if there would be 2-digits notes, we will treat them as one
	let foundDouble = false;
	let stringToReturn = (
		<P.String key={index}>
			{chars.map((char: string, index: number) => {
				if (foundDouble) {
					foundDouble = false;
					return null;
				}

				if (index === 0) {
					return null;
				}

				if (char === '-') {
					return <P.Block className='empty' key={index} />;
				}

				if (!isNaN(parseInt(char))) {
					if (!isNaN(parseInt(chars[index + 1]))) {
						foundDouble = true;
						const value = parseInt(char + chars[index + 1]);
						const result = findNoteNameFromFretboardLocation(defaultNote, value + capoLocation);
						return (
							<P.Block className='note' key={index} isDouble={true}>
								{result}
							</P.Block>
						);
					}

					const value = parseInt(char);
					const result = findNoteNameFromFretboardLocation(defaultNote, value + capoLocation);
					return (
						<P.Block className='note' key={index}>
							{result}
						</P.Block>
					);
				}

				if (char === '|') {
					return <P.Block className='limiter' key={index} />;
				}

				return (
					<P.Block className='other' key={index}>
						{char}
					</P.Block>
				);
			})}
		</P.String>
	);

	return stringToReturn;
};
