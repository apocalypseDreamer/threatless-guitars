import { store } from 'store/config';
import { rootNotes } from 'scales';
import { Instrument, Tunings } from 'api/apiModels';

let notes: string[];
let tunings: Tunings;

store.subscribe(() => {
	const { globals } = store.getState();
	notes = globals.notes;
	tunings = globals.tunings;
});

export const getTuningNotes = (instrument: Instrument, tuningName: string): string[] => {
	const tuningsList = tunings[instrument];

	const tuning = tuningsList.find(({ name }) => name === tuningName);
	if (!tuning) {
		console.warn(`possibly instrument didn't updated yet for tuning:`, tuningName);
	}

	// creating a copy of an existing array and reversing it (the highest string as first)
	return tuning ? [...tuning.notes].reverse() : [];
};

export const findNoteNameFromFretboardLocation = (stringDefaultNote: string, fretNumber: number, nameOnly?: boolean): string => {
	const noteIndex = notes.findIndex((note: string) => stringDefaultNote === note);

	if (nameOnly) {
		const defaultStringName = stringDefaultNote.replace(/\d/, '');
		const defaultStringIndex = rootNotes.findIndex(rootNote => rootNote === defaultStringName);
		const rootNotesStartingFromStringNote = rootNotes.slice(defaultStringIndex).concat(rootNotes.slice(0, defaultStringIndex));

		return rootNotesStartingFromStringNote[fretNumber % 12];
	}

	const note = notes[noteIndex + fretNumber];

	return note;
};
