import { Scale } from 'store/ScaleDrawer/constants';

export const rootNotes = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B'];

export const scales: Scale[] = [
	{ name: 'Major', degree: ['1', '2', '3', '4', '5', '6', '7', '8'], distances: [0, 2, 4, 5, 7, 9, 11] },
	{ name: 'Acoustic', degree: ['1', '2', '3', '#4', '5', '6', 'b7', '8'], distances: [0, 2, 4, 6, 7, 9, 10] },
	{ name: 'Pentatonic Minor', degree: ['1', 'b3', '4', '5', 'b7'], distances: [0, 3, 5, 7, 10] },
];
